<h1>Backend calls:</h1>
- <h2>/update</h2>needs firstname and surname as identifiers and group or uid, values which get updated in the database
- <h2>/read</h2>returs a json with following structure: <p style="color: red;">{ 'uid' : '[red_UID]'}</p>
- <h2>/search</h2><p style="color: green;">type: post</p>receives a json file with surname or firstname or group or uid and sends a json file with the found entries
- <h2>/delete</h2><p style="color: green;">type: post</p> receives a json file with uid and sends a json file with <p style="color: red">{result: Success} or {result: Error}</p>
- <h2>/insert</h2><p style="color: green;">type: post</p>receives a complete filled json with the user data
